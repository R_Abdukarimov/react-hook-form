import Typography from '@mui/material/Typography'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import {
  useForm,
  Controller,
  SubmitHandler,
  useFormState,
} from 'react-hook-form'

import styles from './Login.module.scss'
import { loginValidation, passValidation } from 'utils/validation'

interface ISingnInForm {
  login: string
  password: string
}

const Login: React.FC = () => {
  const { handleSubmit, control } = useForm<ISingnInForm>()
  const onSubmit: SubmitHandler<ISingnInForm> = (data) => console.log(data)
  const { errors } = useFormState({ control })


  return (
    <div className={styles.root}>
      <Typography
        className={styles.login}
        component='div'
        variant='h4'
        gutterBottom
      >
        Войдите
      </Typography>
      <Typography
        className={styles.subtitle}
        component='div'
        variant='subtitle1'
        gutterBottom
      >
        Для получения доступа
      </Typography>

      <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
        <Controller
          control={control}
          name='login'
          rules={loginValidation}
          render={({ field }) => (
            <TextField
              label='Login'
              size='small'
              className={styles.input}
              margin='normal'
              fullWidth={true}
              onChange={(e) => field.onChange(e)}
              value={field.value}
              error={!!errors.login?.message}
              helperText={errors.login?.message}
            />
          )}
        />
        <Controller
          control={control}
          name='password'
          rules={passValidation}
          render={({ field }) => (
            <TextField
              label='Password'
              type='password'
              size='small'
              className={styles.input}
              margin='normal'
              fullWidth={true}
              onChange={(e) => field.onChange(e)}
              value={field.value}
              error={!!errors.password?.message}
              helperText={errors.password?.message}
            />
          )}
        />

        <Button
          type='submit'
          fullWidth={true}
          disableElevation={true}
          variant='contained'
          sx={{
            marginTop: 2,
          }}
        >
          Войти
        </Button>
      </form>
    </div>
  )
}

export default Login
