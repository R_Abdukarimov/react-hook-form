import AuthPage from 'pages/auth'
import './App.scss'
const App: React.FC = () => {
  return (
    <div className='App'>
      <AuthPage />
    </div>
  )
}

export default App
