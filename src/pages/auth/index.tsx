import Login from 'components/Login/Login'
import styles from './AuthPage.module.scss'

const AuthPage: React.FC = () => {
  return (
    <div className={styles.root}>
      <Login />
    </div>
  )
}

export default AuthPage
