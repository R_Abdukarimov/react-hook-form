const REQUIRED_FIELD = 'Обязательное поле'

export const loginValidation = {
  required: REQUIRED_FIELD,
  validate: (value: string) => {
    if (value.match(/[а-яА-Я]/)) {
      return 'Логин не может содержать русские буквы'
    }
    return true
  },
}

export const passValidation = {
  required: REQUIRED_FIELD,
  validate: (value: string) => {
    if (value.match(/[а-яА-Я]/)) {
      return 'Пароль не может содержать русские буквы'
    }
    if (value.length < 6) {
      return 'Пароль должен быть длиннее 6 симолов'
    }
    return true
  },
}
